from string import ascii_uppercase


class Rotor:
    def __init__(self):
        """

        """
        self._rotor_type = None   # Walzenlage
        self._turnover = []
        self._initial_rotor_setting = None
        self._count = 26
        self._matrix = []
        self._position = None

    def initialise(self, rotor_type, turnover, initial_rotor_settings, position):
        """_position

        :param rotor_type:
        :param turnover:
        :param initial_rotor_settings:
        :param position:
        :return:
        """
        self._rotor_type = rotor_type
        self._turnover.append(turnover)
        self._initial_rotor_setting = initial_rotor_settings
        self._position = position

        [self._matrix.append(char) for char in ascii_uppercase]
        # print(self._matrix)

        [self._matrix.append(self._matrix.pop(0)) for char in range(ascii_uppercase.index(initial_rotor_settings))]
        # print(self._matrix)

    def get_io_to_reflector(self, plaintext):
        """
        Encryption io from EntryWheel to Reflector
        :param plaintext:
        :return:
        """
        cipher = self._matrix[ascii_uppercase.index(plaintext)]
        # print('InputChar: ', input, '\tPosition: ', ascii_uppercase.index(input))
        # print('OutputChar: ', output, '\tPosition: ', ascii_uppercase.index(input))

        return cipher

    def get_io_from_reflector(self, plaintext):
        """
        Encryption io from Reflector to EntryWheel
        :param input:
        :return:
        """
        cipher = ascii_uppercase[self._matrix.index(plaintext)]
        # print('InputChar: ', input, '\tPosition: ', self._matrix.index(input))
        # print('OutputChar: ', output, '\tPosition: ', self._matrix.index(input))

        return cipher

    def turnover(self):
        self._matrix.append(self._matrix.pop(0))
        # print(self._matrix)

    def encrypt_to_reflector(self, plaintext):
        if self._position == 'RIGHT':
            self.turnover()
            cipher = self._matrix[ascii_uppercase.index(plaintext)]

        else:
            if self._matrix[0] == self._turnover:
                self.turnover()
                cipher = self._matrix[ascii_uppercase.index(plaintext)]
            else:
                cipher = self._matrix[ascii_uppercase.index(plaintext)]
        return cipher

    def encrypt_to_entrywheel(self, plaintext):
        cipher = ascii_uppercase[self._matrix.index(plaintext)]
        return cipher

    def decrypt_to_reflector(self):
        pass

    def decrypt_to_entrywheel(self):
        pass
