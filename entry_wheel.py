from string import ascii_uppercase


class EntryWheel:
    def __init__(self):
        """
        Enigma - Entry Wheel Testing
        Settings: Static, this is not subject to turnover
        Encryption/Decryption: Char input index is used to determine the output char from entry wheel

        Encryption:
        1. Direction: To Reflector
        2. Direction: From Reflector

        Types:
        0. Commercial:
        ['Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M']

        1. Military:
        ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
        """
        self._etw_type = None
        self._matrix = []
        self._qwerty = 'QWERTYUIOPASDFGHJKLZXCVBNM'
        # TODO: Inheritance from entry wheel, called static wheel

    def initialise(self, etw_type):
        """
        Initialise Entry Wheel, static settings
        0. Commercial = qwerty
        1. Military = pass through

        :param _etw_type:
        :return:
        """
        self._etw_type = etw_type

        # TODO: override position
        if self._etw_type == 0:
            for char in self._qwerty:
                self._matrix.append(char)

        else:
            for char in ascii_uppercase:
                self._matrix.append(char)

        # print([char for char in ascii_uppercase])
        # print(len(ascii_uppercase))
        # print(self._matrix)

    def get_io_to_reflector(self, input):
        """
        Encryption io from EntryWheel to Reflector
        :param input:
        :return:
        """
        output = self._matrix[ascii_uppercase.index(input)]

        print('InputChar: ', input, '\tPosition: ', ascii_uppercase.index(input))
        print('OutputChar: ', output, '\tPosition: ', ascii_uppercase.index(input))

        return output

    def get_io_from_reflector(self, input):
        """
        Encryption io from Reflector to EntryWheel

        :return:
        """

        output = ascii_uppercase[self._matrix.index(input)]

        # print('InputChar: ', input, '\tPosition: ', self._matrix.index(input))
        # print('OutputChar: ', output, '\tPosition: ', self._matrix.index(input))

        # TODO: Refactor - inheritance
        return output
