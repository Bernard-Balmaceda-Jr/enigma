from reflector import Reflector
from rotor import Rotor
from entry_wheel import EntryWheel
import string


class Enigma:
    def __init__(self):
        self.reflector = Reflector()
        self.rotor_left = Rotor()
        self.rotor_middle = Rotor()
        self.rotor_right = Rotor()
        self.entry_wheel = EntryWheel()

    def initialise(self):
        self.reflector.initialise()
        self.rotor_left.initialise(rotor_type='I', turnover='W', initial_rotor_settings='R', position='LEFT')
        self.rotor_middle.initialise(rotor_type='II', turnover='N', initial_rotor_settings='A', position='MIDDLE')
        # self.rotor_middle.initialise(rotor_type='II', turnover='N', initial_rotor_settings='N', position='MIDDLE')
        self.rotor_right.initialise(rotor_type='III', turnover='M', initial_rotor_settings='O', position='RIGHT')
        self.entry_wheel.initialise(etw_type=0)

    def static_encrypt_char(self, plaintext):
        """
        Stage:
        1. Keyboard Input
        2. EntryWheel
        3. RotorRight
        4. RotorMiddle
        5. RotorLeft
        6. Reflector

        7. RotorLeft
        8. RotorMiddle
        9. RotorRight
        10. EntryWheel
        11. Output

        return: ciphertext
        """
        ciphertext = None

        #   ETW Input
        print([x for x in string.ascii_uppercase])
        etw_input = self.entry_wheel.get_io_to_reflector(plaintext)
        print(self.entry_wheel._matrix)
        print('Entry Wheel Input: ', etw_input)

        #   Right Input
        rotor_right_input = self.rotor_right.get_io_to_reflector(etw_input)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_right._matrix)
        print("Rotor Right Input: ", rotor_right_input)

        #   Middle Input
        rotor_middle_input = self.rotor_middle.get_io_to_reflector(rotor_right_input)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_middle._matrix)
        print("Rotor Middle Input: ", rotor_middle_input)

        #   Left Input
        rotor_left_input = self.rotor_left.get_io_to_reflector(rotor_middle_input)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_left._matrix)
        print("Rotor Left Input: ", rotor_left_input)

        #   Reflector Output
        reflector_output = self.reflector.get_io_of_reflector(rotor_left_input)
        print([x for x in string.ascii_uppercase])
        print(self.reflector._matrix)
        print("Reflector Output: ", reflector_output)

        #   Left Output
        rotor_left_output = self.rotor_left.get_io_from_reflector(reflector_output)
        print(self.rotor_left._matrix)
        print([x for x in string.ascii_uppercase])
        print("Rotor Left Output: ", rotor_left_output)

        #   Middle Output
        rotor_middle_output = self.rotor_middle.get_io_from_reflector(rotor_left_output)
        print(self.rotor_middle._matrix)
        print([x for x in string.ascii_uppercase])
        print("Rotor Middle Output: ", rotor_middle_output)

        #   Right Output
        rotor_right_output = self.rotor_right.get_io_from_reflector(rotor_middle_output)
        print(self.rotor_right._matrix)
        print([x for x in string.ascii_uppercase])
        print("Rotor Right Output: ", rotor_right_output)

        #   ETW Output
        etw_output = self.entry_wheel.get_io_from_reflector(rotor_right_output)
        print(self.entry_wheel._matrix)
        print([x for x in string.ascii_uppercase])
        print("Entry Wheel Output: ", etw_output)

        ciphertext = etw_output
        return ciphertext

    def static_decrypt_char(self):
        pass

    def encrypt(self, plaintext):
        ciphertext = None

        #   ETW Input
        print([x for x in string.ascii_uppercase])
        etw_input = self.entry_wheel.get_io_to_reflector(plaintext)
        print(self.entry_wheel._matrix)
        print('Entry Wheel Input: ', etw_input)

        #   Right Input
        rotor_right_input = self.rotor_right.encrypt_to_reflector(etw_input)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_right._matrix)
        print("Rotor Right Input: ", rotor_right_input)

        #   TODO: Middle Input
        rotor_middle_input = self.rotor_middle.encrypt_to_reflector(rotor_right_input)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_middle._matrix)
        print("Rotor Middle Input: ", rotor_middle_input)

        #   TODO: Left Input
        rotor_left_input = self.rotor_left.encrypt_to_reflector(rotor_middle_input)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_left._matrix)
        print("Rotor Left Input: ", rotor_left_input)

        #   TODO: Reflector Output
        reflector_output = self.reflector.get_io_of_reflector(rotor_left_input)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_left._matrix)
        print("Rotor Middle Input: ", reflector_output)

        #   TODO: Left Output
        rotor_left_output = self.rotor_left.encrypt_to_entrywheel(reflector_output)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_left._matrix)
        print("Rotor Left Output: ", rotor_left_output)

        #   TODO: Middle Output
        rotor_middle_output = self.rotor_middle.encrypt_to_entrywheel(rotor_left_output)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_middle._matrix)
        print("Rotor Middle Output: ", rotor_middle_output)

        #   TODO: Right Output
        rotor_right_output = self.rotor_right.encrypt_to_entrywheel(rotor_middle_output)
        print([x for x in string.ascii_uppercase])
        print(self.rotor_right._matrix)
        print("Rotor Right Output: ", rotor_right_output)

        #   TODO: ETW Output
        ETW_output = self.entry_wheel.get_io_from_reflector(rotor_right_output)
        print([x for x in string.ascii_uppercase])
        print(self.entry_wheel._matrix)
        print("Rotor Right Output: ", ETW_output)

        ciphertext = ETW_output
        return ciphertext


def main():
    # TODO: Initialise Enigma
    enigma = Enigma()
    enigma.initialise()

    char1 = input("Input First Char: ")

    print('Enigma RightTurnOver: ', enigma.rotor_right._turnover[0] + '\tInitSettings: ', enigma.rotor_right._initial_rotor_setting)
    print('Enigma MiddleTurnOver: ', enigma.rotor_middle._turnover[0] + '\tInit: ', enigma.rotor_middle._initial_rotor_setting)
    print('Enigma LeftTurnOver: ', enigma.rotor_left._turnover[0] + '\tInit: ', enigma.rotor_left._initial_rotor_setting)

    print('Static Output')
    output = enigma.static_encrypt_char(char1)
    print(output)

    print('Dynamic Output')
    output = enigma.encrypt(char1)
    print(output)

    # plaintext = input("Input First Char: ")
    # cipher = enigma.encrypt(plaintext)
    # print(cipher)
    #
    # plaintext2 = input("Test again: ")
    # cipher2 = enigma.encrypt(plaintext2)
    # print(cipher2)


if __name__ == '__main__':
    main()
