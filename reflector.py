from string import ascii_uppercase


class Reflector:
    def __init__(self):
        """
        Modelled against: Enigma B model
        """
        self._model = "Enigma_B"
        self._matrix = []
        self._reflector = 'QYHOGNECVPUZTFDJAXWMKISRBL'

        # TODO: Inheritance from entry wheel, called static wheel

    def initialise(self):
        for char in self._reflector:
            self._matrix.append(char)

        # print(self._matrix)
        # print([char for char in ascii_uppercase])

    def get_io_of_reflector(self, input):
        """

        :param input:
        :return:
        """
        output = self._matrix[ascii_uppercase.index(input)]

        # print('InputChar: ', input, '\tPosition: ', ascii_uppercase.index(input))
        # print('OutputChar: ', output, '\tPosition: ', ascii_uppercase.index(input))

        return output
