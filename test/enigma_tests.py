import pytest
from enigma import Enigma


@pytest.fixture
def etw_comm():
    """
    Enigma with Commercial Entry Wheel
    :return:
    """
    etw_comm = Enigma()
    etw_comm.entry_wheel.initialise(0)
    etw_comm.rotor_right.initialise('I', 'M', 'M', 'RIGHT')
    etw_comm.rotor_middle.initialise('II', 'A', 'T', 'MIDDLE')
    etw_comm.rotor_left.initialise('III', 'A', 'K' , 'LEFT')
    etw_comm.reflector.initialise()
    return etw_comm


@pytest.fixture
def etw_mil():
    """
    Enigma with Military Entry Wheel
    :return:
    """
    etw_mil = Enigma()
    etw_mil.entry_wheel.initialise(1)
    etw_mil.rotor_right.initialise('III', 'W', 'M', 'RIGHT')
    etw_mil.rotor_middle.initialise('II', 'F', 'T', 'MIDDLE')
    etw_mil.rotor_left.initialise('I', 'R', 'K', 'LEFT')
    etw_mil.reflector.initialise()
    return etw_mil


@pytest.fixture
def custom_rotors(
        type_right='III', turnover_right='M', initial_right='O', right_position='RIGHT',
        type_middle='II', turnover_middle='N', initial_middle='A', middle_position='MIDDLE',
        type_left='I', turnover_left='W', initial_left='R', left_position='LEFT'
    ):
    """
    Enigma with Customized Rotor Right Settings
    :return:
    """
    etw_mil = Enigma()
    etw_mil.entry_wheel.initialise(1)
    etw_mil.rotor_right.initialise(type_right, turnover_right, initial_right, right_position)
    etw_mil.rotor_middle.initialise(type_middle, turnover_middle, initial_middle, middle_position)
    etw_mil.rotor_left.initialise(type_left, turnover_left, initial_left, left_position)
    etw_mil.reflector.initialise()
    return etw_mil


def test_reflector(etw_mil):
    """
    Reflector

    German Railway (Rocket):
    ['Q','Y','H','O','G','N','E','C','V','P','U','Z','T','F','D','J','A','X','W','M','K','I','S','R','B','L']

    Index:
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
reflector_output
    :param etw_mil:
    :return:
    """
    #   Entry Wheel Militar Spec - Tests
    #   Direction from ETW -> To Reflector
    input_1 = 'A'
    input_2 = 'B'
    input_3 = 'Z'
    output_1 = etw_mil.reflector.get_io_of_reflector(input_1)
    output_2 = etw_mil.reflector.get_io_of_reflector(input_2)
    output_3 = etw_mil.reflector.get_io_of_reflector(input_3)
    assert output_1 == 'Q'
    assert output_2 == 'Y'
    assert output_3 == 'L'


def test_rotor_left_middle_right(custom_rotors):
    """
    Static matrix test, to see if the index of input char, when used as index of specified rotor with respect to
    initial settings, is translated into the correct char:
    * rotor_right = O
    * rotor_middle = A
    * rotor_left = R


    Position of turnover notches
    Rotor:      Turnover:
    I           R
    II          F
    III         W
    IV          K
    V           A
    VI          A and N
    VII         A and N
    VIII        A and N

    Test Settings
    rotor_type:III
    turnover: "W"
    initial_rotor_settings: "O"

    Rotor Right: Initial setting O
    ['O','P','Q','R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N']

    Rotor Middle: Initial setting A
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

    Rotor Left: Initial settings R
    ['R','S','T','U','V','W','X','Y','Z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O',P','Q']

    Input Index:
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    """
    #   Direction from ETW -> To Reflector
    input_1 = 'A'
    input_2 = 'B'
    input_3 = 'Z'
    output_1_right = custom_rotors.rotor_right.get_io_to_reflector(input_1)
    output_2_right = custom_rotors.rotor_right.get_io_to_reflector(input_2)
    output_3_right = custom_rotors.rotor_right.get_io_to_reflector(input_3)

    output_1_middle = custom_rotors.rotor_middle.get_io_to_reflector(input_1)
    output_2_middle = custom_rotors.rotor_middle.get_io_to_reflector(input_2)
    output_3_middle = custom_rotors.rotor_middle.get_io_to_reflector(input_3)

    output_1_left = custom_rotors.rotor_left.get_io_to_reflector(input_1)
    output_2_left = custom_rotors.rotor_left.get_io_to_reflector(input_2)
    output_3_left = custom_rotors.rotor_left.get_io_to_reflector(input_3)

    assert output_1_right == 'O'
    assert output_2_right == 'P'
    assert output_3_right == 'N'
    assert output_1_middle == 'A'
    assert output_2_middle == 'B'
    assert output_3_middle == 'Z'
    assert output_1_left == 'R'
    assert output_2_left == 'S'
    assert output_3_left == 'Q'

    print(custom_rotors.rotor_right._matrix)
    print(custom_rotors.rotor_middle._matrix)
    print(custom_rotors.rotor_left._matrix)

    #   Direction from Reflector to ETW
    input_4_right = 'O'
    input_5_right = 'P'
    input_6_right = 'N'
    input_4_middle = 'A'
    input_5_middle = 'B'
    input_6_middle = 'Z'
    input_4_left = 'R'
    input_5_left = 'S'
    input_6_left = 'Q'

    output_4_right = custom_rotors.rotor_right.get_io_from_reflector(input_4_right)
    output_5_right = custom_rotors.rotor_right.get_io_from_reflector(input_5_right)
    output_6_right = custom_rotors.rotor_right.get_io_from_reflector(input_6_right)

    output_4_middle = custom_rotors.rotor_middle.get_io_from_reflector(input_4_middle)
    output_5_middle = custom_rotors.rotor_middle.get_io_from_reflector(input_5_middle)
    output_6_middle = custom_rotors.rotor_middle.get_io_from_reflector(input_6_middle)

    output_4_left = custom_rotors.rotor_left.get_io_from_reflector(input_4_left)
    output_5_left = custom_rotors.rotor_left.get_io_from_reflector(input_5_left)
    output_6_left = custom_rotors.rotor_left.get_io_from_reflector(input_6_left)
    assert output_4_right == 'A'
    assert output_5_right == 'B'
    assert output_6_right == 'Z'
    assert output_4_middle == 'A'
    assert output_5_middle == 'B'
    assert output_6_middle == 'Z'
    assert output_4_left == 'A'
    assert output_5_left == 'B'
    assert output_6_left == 'Z'


def test_etw_com_spec(etw_comm, etw_mil):
    """
    Entry Wheel Tests (ETW): Commercial Entry Wheel

    0. Commercial Spec:
    ['Q','W','E','R','T','Y','U','I','O','P','A','S','D','F','G','H','J','K','L','Z','X','C','V','B','N','M']

    index
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    """

    #   Entry Wheel Commercial Spec - Tests
    #   TODO: input guard upper

    #   Direction from ETW -> To Reflector
    input_1 = 'A'
    input_2 = 'B'
    input_3 = 'Z'
    output_1 = etw_comm.entry_wheel.get_io_to_reflector(input_1)
    output_2 = etw_comm.entry_wheel.get_io_to_reflector(input_2)
    output_3 = etw_comm.entry_wheel.get_io_to_reflector(input_3)
    assert output_1 == 'Q'
    assert output_2 == 'W'
    assert output_3 == 'M'

    #   Direction from Reflector to ETW
    input_4 = 'Q'
    input_5 = 'W'
    input_6 = 'M'
    output_4 = etw_comm.entry_wheel.get_io_from_reflector(input_4)
    output_5 = etw_comm.entry_wheel.get_io_from_reflector(input_5)
    output_6 = etw_comm.entry_wheel.get_io_from_reflector(input_6)
    assert output_4 == 'A'
    assert output_5 == 'B'
    assert output_6 == 'Z'


def test_etw_mil_spec(etw_mil):
    """
    Entry Wheel Tests (ETW): Military Entry Wheel

    1. Military Spec:
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']

    index
    ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z']
    """
    #   Entry Wheel Militar Spec - Tests
    #   Direction from ETW -> To Reflector
    input_1 = 'A'
    input_2 = 'B'
    input_3 = 'Z'
    output_1 = etw_mil.entry_wheel.get_io_to_reflector(input_1)
    output_2 = etw_mil.entry_wheel.get_io_to_reflector(input_2)
    output_3 = etw_mil.entry_wheel.get_io_to_reflector(input_3)
    assert output_1 == 'A'
    assert output_2 == 'B'
    assert output_3 == 'Z'

    #   Direction from Reflector to ETW
    input_4 = 'A'
    input_5 = 'B'
    input_6 = 'Z'
    output_4 = etw_mil.entry_wheel.get_io_from_reflector(input_4)
    output_5 = etw_mil.entry_wheel.get_io_from_reflector(input_5)
    output_6 = etw_mil.entry_wheel.get_io_from_reflector(input_6)
    assert output_4 == 'A'
    assert output_5 == 'B'
    assert output_6 == 'Z'



